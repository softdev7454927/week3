/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3_3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Lenovo
 */
public class XOProgramUnitTest {
    
    public XOProgramUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testCheckWinNoPlayBY_O_output_false() {
        String[][] table = {{"-","-","-"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "O";
        assertEquals(false,OXProgram.checkWin(table,currentPlayer));
}
    
    @Test
    public void testCheckWinRow2PlayBY_O_output_true() {
        String[][] table = {{"-","-","-"},{"O","O","O"},{"-","-","-"}};
        String currentPlayer = "O";
        assertEquals(true,OXProgram.checkWin(table,currentPlayer));
}
    @Test
    public void testCheckWinRow1PlayBY_X_output_true() {
        String[][] table = {{"X","X","X"},{"-","O","O"},{"-","-","-"}};
        String currentPlayer = "X";
        assertEquals(true,OXProgram.checkWin(table,currentPlayer));
}
    @Test
    public void testCheckWinRow3PlayBY_X_output_true() {
        String[][] table = {{"-","O","X"},{"-","O","O"},{"X","X","X"}};
        String currentPlayer = "X";
        assertEquals(true,OXProgram.checkWin(table,currentPlayer));
}
    @Test
    public void testCheckWinCol2PlayBY_O_output_true() {
        String[][] table = {{"-","O","-"},{"X","O","X"},{"X","O","-"}};
        String currentPlayer = "O";
        assertEquals(true,OXProgram.checkWin(table,currentPlayer));
}
    @Test
    public void testCheckWinCol3PlayBY_X_output_true() {
        String[][] table = {{"-","O","X"},{"-","O","X"},{"O","-","X"}};
        String currentPlayer = "X";
        assertEquals(true,OXProgram.checkWin(table,currentPlayer));
}
    @Test
    public void testCheckWinCol1PlayBY_X_output_true() {
        String[][] table = {{"X","O","X"},{"X","-","O"},{"X","-","O"}};
        String currentPlayer = "X";
        assertEquals(true,OXProgram.checkWin(table,currentPlayer));
}
    @Test
    public void testCheckWinDiagonalsPlayBY_X_output_true() {
        String[][] table = {{"X","O","-"},{"O","X","X"},{"O","-","X"}};
        String currentPlayer = "X";
        assertEquals(true,OXProgram.checkWin(table,currentPlayer));
}
    @Test
    public void testCheckWinDiagonalsPlayBY_O_output_true() {
        String[][] table = {{"X","-","O"},{"-","O","X"},{"O","-","X"}};
        String currentPlayer = "O";
        assertEquals(true,OXProgram.checkWin(table,currentPlayer));
}
    @Test
    public void testCheckDraw_output_false() {
        String[][] table = {{"O","X","X"},{"X","X","O"},{"O","O","X"}};
        String currentPlayer = "X";
        assertEquals(false,OXProgram.checkWin(table,currentPlayer));
}
    @Test
    public void testCheckDraw2_output_true() {
        String[][] table = {{"O","X","X"},{"X","X","O"},{"O","O","X"}};
        String currentPlayer = "X";
        assertEquals(true,OXProgram.checkDraw2(table,currentPlayer));
}
}
