/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab3_3;

/**
 *
 * @author Lenovo
 */
class OXProgram {
    static boolean checkWin(String[][] table, String currentPlayer) {
        if (checkRow(table, currentPlayer)) {
            return true;
        }
        if (checkCol(table, currentPlayer)) {
            return true;
        }
        if (checkDiagonals(table, currentPlayer)) {
            return true;
        }
        if (checkDraw(table, currentPlayer)) {
            return false;
        }
        
       return false;
    }
    
    private static boolean checkRow(String[][] table, String currentPlayer) {
        for (int row = 0; row<3; row++) {
            if (checkRow(table, currentPlayer, row)) return true;
        }
        return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer, int row) {
        for (int i = 0; i < 3; i++) {
            if (table[row][i] != currentPlayer) {
                return false;
            }
        }
        return true;

    }
    private static boolean checkCol(String[][] table, String currentPlayer) {
        for (int col = 0; col<3; col++) {
            if (checkCol(table, currentPlayer, col)) return true;
        }
        return false;
    }

    private static boolean checkCol(String[][] table, String currentPlayer, int col) {
        for (int i = 0; i < 3; i++) {
            if (table[i][col] != currentPlayer) {
                return false;
            }
        }
        return true;

    }

    private static boolean checkDiagonals(String[][] table, String currentPlayer) {
        if (table[0][0] == currentPlayer && table[1][1] == currentPlayer && table[2][2] == currentPlayer) {
            return true;
        } else if (table[0][2] == currentPlayer && table[1][1] == currentPlayer && table[2][0] == currentPlayer) {
            return true;
        }
        return false;
    }

    private static boolean checkDraw(String[][] table, String currentPlayer) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == "-") {
                    return false;
                }
            }
        }
        return true;
    }
    
    static boolean checkDraw2(String[][] table, String currentPlayer) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == "-") {
                    return false;
                }
            }
        }
        return true;
    }
}
